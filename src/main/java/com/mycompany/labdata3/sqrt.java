/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labdata3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class sqrt {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
       int num = sc.nextInt();
       int ans = sqrt(num);
        System.out.println(ans);
    }

    private static int sqrt(int num) {
        if(num==0||num==1){
            return num;
        }
        int result = 0;
        while(result*result<=num){
             result++;
        }
        result =result -1;
        return result;
    }
}
